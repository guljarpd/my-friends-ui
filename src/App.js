import React from 'react';
// import logo from './logo.svg';
import Components from './components';
import './App.css';

function App() {
  return (
    <div className="App">
      <Components />
    </div>
  );
}

export default App;
