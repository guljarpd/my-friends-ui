import React from 'react';
import Pagination from '@material-ui/lab/Pagination';
import styled from 'styled-components';

const StyledPagination = styled(Pagination)`
  text-align: center;
  padding: 10px 0;
`

export default function PaginationFn({totalCount, page, onPageSelect}) {

  function handleChange (event, value) {
    onPageSelect(value);
  }

  return (
    <div>
      <StyledPagination
        count={totalCount}
        page={page}
        onChange={handleChange}
        color="primary"
      />
    </div>
  );
}
