import React from 'react';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
//
import puzzle from '../img/Puzzle-01.svg';


export default function TheirFriends({data}) {
  //
  // console.log('data', data);
  //
  // function onUserSelect(id) {
  //   clickedId(id);
  // }

  return (
    <React.Fragment>
      <Paper>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h5">Their Friends</Typography>
          </Toolbar>
        </AppBar>
        <List dense>
          {data.map((value) => {
            const labelId = `checkbox-list-secondary-label-${value.id}`;
            return (
              <ListItem key={value.id} button>
                <ListItemAvatar>
                  <Avatar
                    alt={value.firstname}
                    src={value.avatar}
                  />
                </ListItemAvatar>
                <ListItemText id={labelId} primary={`${value.firstname} ${value.lastname}`} />
              </ListItem>
            );
          })}
        </List>
        {/*placeholder image*/}
        {data.length === 0? <img src={puzzle} alt={'Placeholder'} />: null}
      </Paper>
    </React.Fragment>
  )
}
