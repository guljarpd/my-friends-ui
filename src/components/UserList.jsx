import React from 'react';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
//
import Pagination from './Pagination';


export default function UserList({data, clickedId, clickedPage, totalPageCount, pageNo}) {
  //
  // console.log('data', data);
  //
  function onUserSelect(id) {
    clickedId(id);
  }
  //
  function _onPageSelect(pNo) {
    if(!pNo) return;
    clickedPage(pNo);
  }

  // let data = params.data;

  return (
    <React.Fragment>
      <Paper>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h5">Users List</Typography>
          </Toolbar>
        </AppBar>
        <List dense>
          {data.map((value) => {
            const labelId = `checkbox-list-secondary-label-${value.id}`;
            return (
              <ListItem key={value.id} button onClick={()=> onUserSelect(value.id)}>
                <ListItemAvatar>
                  <Avatar
                    alt={value.firstname}
                    src={value.avatar}
                  />
                </ListItemAvatar>
                <ListItemText id={labelId} primary={`${value.firstname} ${value.lastname}`} />
              </ListItem>
            );
          })}
        </List>
        <Pagination
          totalCount = {totalPageCount}
          page = {pageNo}
          onPageSelect = {_onPageSelect}
        />
      </Paper>
    </React.Fragment>
  )
}
