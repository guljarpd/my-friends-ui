import React, {useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
//
import UserList from './UserList';
import Friends from './FriendsList';
import TheirFriends from './TheirFriendsList';
//
import {
  fetchUserList,
  fetchFriendsList
} from '../services/api';

//
export default function Index() {
  // set all users list data in local store
  const [userData, setUserData] = React.useState([]);
  // set all friends list data
  const [myFriendsData, setMyFriendsData] = React.useState([]);
  // set all friends of friends list data
  const [theirFriendsData, setTheirFriendsData] = React.useState([]);
  // page count. set default is 1
  const [pageNo, setPageNo] = React.useState(1);
  //
  const totalPageCount = 5;

  // when user selected.
  function selectedUser(id){
    // console.log('selectedUser', id);
    getFriendsList(id);
  }

  // when friends selected.
  function selectedFriend(id){
    // console.log('selectedFriend', id);
    getTheirFriendsList(id);
  }

  // when page number selecting
  function selectedPageNo(pNo){
    // console.log('selectedPageNo', pNo);
    setPageNo(pNo);
    getUserList(pNo);
  }
  // user api call
  async function getUserList(pageNo){
    try {
      let result = await fetchUserList(pageNo);
      // console.log('result', result);
      if (result && result.data.length > 0) {
        setUserData(result.data);
      }
    } catch (e) {
      // console.log('getUserList', e);
      return;
    }
  }
  // friends api call
  async function getFriendsList(uId){
    try {
      let result = await fetchFriendsList(uId);
      // console.log('result', result);
      if (result && result.data.length > 0) {
        setMyFriendsData(result.data);
      }
    } catch (e) {
      // console.log('getUserList', e);
      return;
    }
  }
  //
  // their friends api call
  async function getTheirFriendsList(uId){
    try {
      let result = await fetchFriendsList(uId);
      // console.log('result', result);
      if (result && result.data.length > 0) {
        setTheirFriendsData(result.data);
      }
    } catch (e) {
      // console.log('getUserList', e);
      return;
    }
  }
  // run only on page load. passing []
  useEffect(() => {
    // get 1 page data.
    getUserList(1);
  }, []);

  //
  return (
    <Grid container spacing={3}>
      <Grid item key={1} xs={12} sm={4}>
        <UserList
          data = {userData}
          clickedId = {selectedUser}
          clickedPage = {selectedPageNo}
          totalPageCount = {totalPageCount}
          pageNo = {pageNo}
        />
      </Grid>
      <Grid item key={2} xs={12} sm={4}>
        <Friends
          data = {myFriendsData}
          clickedId = {selectedFriend}
        />
      </Grid>
      <Grid item key={3} xs={12} sm={4}>
        <TheirFriends
          data = {theirFriendsData}
        />
      </Grid>
    </Grid>
  );
}
