const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
// console.log('API_BASE_URL', API_BASE_URL);

// fetch users list
export const fetchUserList = async (pageNo) => {
  const response = await fetch(API_BASE_URL+'/api/v1/users/list/' + pageNo);
  // console.log(response);
  return response.json();
}
// fetch friends list
export const fetchFriendsList = async (userId) => {
  const response = await fetch(API_BASE_URL+'/api/v1/friends/list/' + userId);
  // console.log(response);
  return response.json();
}
